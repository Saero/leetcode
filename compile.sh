!#/bin/bash

# TO RUN: source compile.sh

cd CMakeFiles
cmake .
make -j16
cd ..

# This is needed to allow auto complete with FetchContent
path=$(pwd)
if [[ ":$PATH:" != *":$path"* ]]; then
  export PATH=$PATH:$path
fi
