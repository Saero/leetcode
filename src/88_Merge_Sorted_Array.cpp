#include <iostream>
#include <vector>

using namespace std;//bad code, but needed as that is what leetcode operates on
void merge(std::vector<int>& nums1, int m, std::vector<int>& nums2, int n)
{
	if(n<=0) // edge case - no op
		return;
	if(m<0) // edge case - nothing in nums1
	{
		nums1 = nums2;
		return;
	}
	--m;
	--n;
	for(int iter=int(nums1.size())-1; iter >= 0; --iter)
	{
		if(nums1[m] > nums2[n])
		{
			nums1[iter] = nums1[m];
			if(m != iter)
				nums1[m] = -1111111;//should be std::numeric_limits<int>::min() ... but that would raise my memory usage
			if(m>0)
				--m;
		}
		else
		{
			nums1[iter] = nums2[n];
			nums2[n] = -111111;
			if(n>0)
				--n;
		}

		std::cout<<"\ncurrent nums1\n";
		for(auto i: nums1)
			std::cout<<" "<<i;
		std::cout<<"\n";
	}
}

int main(int argc, char *argv[])
{
	//std::vector<int> nums1{1,2,3,0,0,0};
	//int m = 3;
	//std::vector<int> nums2{2,5,6};
	//int n = 3;

	std::vector<int> nums1{2,0};
	int m = 1;
	std::vector<int> nums2{1};
	int n = 1;
	
	merge(nums1, m, nums2, n);		
	return 0;
}
