#include <gtest/gtest.h>
#include <utility>
#include <iostream>
#include <string>

template <typename T>
class my_shared_ptr
{
public:
  // TODO: add default/parameterized constructors
  my_shared_ptr(){}
  my_shared_ptr(T& obj_) : obj(obj_), obj_ptr(&obj) { }
  my_shared_ptr(T* obj_) { 
    obj = *obj_; 
    obj_ptr = &obj;
  } // TODO properly transfer memory, don't copy
  // TODO: add move/copy constructors and assignment operators

  T* get() const
  {
    return obj_ptr; // shouldn't need to cast out const
  }

  ~my_shared_ptr()
  {
    // TODO: implement
    // only trigger if ref count == 0
  }

  T& operator*() const // classic deref
  {
    return *obj_ptr;
  }

  T* operator->() const // shared deref
  {  
    // TODO: implement
  }

  private:
    uint refCount{};
    T obj{};
    T* obj_ptr{};
};

//TEST(Basic_construction, my_shared_part_1)
//{
//  my_shared_ptr<int> a;
//  EXPECT_EQ(a.get() == nullptr, true);
//  
//  my_shared_ptr<int> b{new int{15}};
//  EXPECT_EQ(b.get() != nullptr, true);
//  EXPECT_EQ(*b == 15, true);
//}
//
//TEST(Copy_construction_and_assignment, my_shared_part_2)
//{
//  my_shared_ptr<int> a{new int{3}};
//  my_shared_ptr<int> b{a};
//  EXPECT_EQ(b.get() == a.get(), true);
//
//  my_shared_ptr<int> c;
//  c = b;
//  EXPECT_EQ(c.get() == b.get(), true);
//}

TEST(Move_construction_and_assignment, my_shared_part_3) {
  my_shared_ptr<int> a{new int{1}};
  //EXPECT_EQ(a.get() != nullptr, true);
  
  auto* underlying = a.get();
  my_shared_ptr<int> b{std::move(a)};
  EXPECT_EQ(a.get() == nullptr, true);
  EXPECT_EQ(b.get() == underlying, true);

  my_shared_ptr<int> c;
  c = std::move(b);
  EXPECT_EQ(b.get() == nullptr, true);
  EXPECT_EQ(c.get() == underlying, true);
}
