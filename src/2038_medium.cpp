#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

int flipgame(vector<int>& fronts, vector<int>& backs){
    // Well this is made into an O(n) problem because I am guaranteed that there will always be two colors preventing "bridges" by other players
    int size = colors.size();//NOTE TO SELF: size() isn't cached and it has a lot of overhead
    if(size < 3)
        return false;// Bob will always win in this case
    int turns = 0; // if positive, Alice has more turns, if negative or 0, Bob wins because he has more turns
    for(int iter = 1; iter < size; ++iter){
        if(colors[iter-1] == colors[iter+1] && colors[iter] == colors[iter-1]){
            if(colors[iter] == 'A'){
                ++turns;
            }
            else{
                --turns;
            }
        }
    }
    if(turns<=0){
        return false;
    }
    return true;
}

TEST(l2038, starter_example){
    auto colors = string{"AAABABB"};
    auto result = winnerOfGame(colors);
    EXPECT_EQ(result, true);
}

TEST(l2038, starter_example){
    auto colors = string{"AA"};
    auto result = winnerOfGame(colors);
    EXPECT_EQ(result, true);
}

TEST(l2038, starter_example){
    auto colors = string{"ABBBBBBBAAA"};
    auto result = winnerOfGame(colors);
    EXPECT_EQ(result, true);
}
