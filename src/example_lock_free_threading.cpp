// Credit for code: youtube Multithreading | Lock Free Programming by Coding Interview Prep
// example use: exe ABCDEFGHIJ 3 4
// Expected output:
// Thread1: ABC
// Thread2: DEF
// Thread3: GHI
// Thread4: JAB
// Thread1: CDE
// Thread2: FGH
// Thread3: IJA

#include <atomic>
#include <charconv>
#include <chrono>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std;//yes I know this is terrible practice in a .h file, probably the same here too, but I am trying to get used to coding in leetcode

std::atomic<uint32_t> threadTurn{};
std::size_t currIndex{};
void printChars(string& str, size_t charCount, size_t myTurn, uint32_t threadCount){
  bool notDone{true};
  while(notDone){
    while(threadTurn.load(std::memory_order_acquire) != myTurn){}

    cout<<"Thread "<<myTurn + 1<<": ";

    auto index = currIndex;
    for(size_t i = 0; i < charCount; ++i){
      cout<<str[index++];
      index = (index == str.length()) ? 0 : index;
    }
    cout << endl;

    currIndex = index;

    uint32_t nextTurn = myTurn + 1;
    nextTurn = (nextTurn == threadCount) ? 0 : nextTurn;
    threadTurn.store(nextTurn, memory_order_release);
    notDone = false;
  }
}


int main(int agrc, char** argv) {
  if(agrc < 4){
    cerr << "\nNot enough arguements! " << endl;
    exit(1);
  }

  string str{argv[1]};
  size_t charCount;
  char* argv2{argv[2]};
  if(auto [ptr, ec] = from_chars(argv2, argv2 + strlen(argv[2]), charCount); ec != std::errc()){
    std::cerr << "Unable to aprse char count. Error: " << std::make_error_code(ec).message() << std::endl;
    exit(1);
  }

  if(charCount == 0){
    std::cerr << "char count must be greater than 0"<<endl;
  }

  uint32_t threadCount{};
  char* argv3{argv[3]};
  if (auto [ptr, cc] = std::from_chars(argv3, argv3 + strlen(argv[3]), threadCount); cc != std::errc()){
    cerr << "Unable to parse thread count. Error: " << std::make_error_code(cc) << endl;
    exit(1);
  }
 
  if(threadCount == 0){
    std::cerr << "thread count must be greater than 0"<<endl;
  }


  vector<thread> threads;
  for(size_t i = 0; i < threadCount; ++i){
    threads.emplace_back( [myTurn=i, charCount=charCount, threadCount=threadCount, &str]() {printChars(str, charCount, myTurn, threadCount); });
  }

  using namespace std::chrono_literals;
  this_thread::sleep_for(1000ms);
  
  for(auto& thread: threads){
    thread.join();
  }


  return 0;
}
