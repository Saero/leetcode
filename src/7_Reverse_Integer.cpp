#include <cstdint>
#include <iostream> 
#include <limits>
#include <vector>
#include <algorithm>
#include <numeric>
#include <string>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

class Solution {
public:
  int reverse (int x){
    if(x < std::numeric_limits<int32_t>::min() || x > std::numeric_limits<int32_t>::max()) 
      return 0;
    bool isNegative{x < 0};
    string xString(to_string(x));
    std::reverse(xString.begin(), xString.end());
    long xtemp = stoll(xString);
    if(xtemp > std::numeric_limits<int32_t>::max()) // post conversion might go out of bounds
      return 0;
    x = xtemp;
    if(isNegative){
      x *= -1;
    }
    return x;
  }
};

TEST(l2038, starter_example){
    Solution solution{};
    auto result = solution.reverse(123);
    EXPECT_EQ(result, 321);
    
  result = solution.reverse(-123);
    EXPECT_EQ(result, -321);

    result = solution.reverse(1534236469); // post conversion is out of bounds
    EXPECT_EQ(result, 0);
}
