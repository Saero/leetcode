#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

class Solution{
public:
  vector<int> spiralOrder(vector<vector<int>>& matrix){
    auto matrix_size = matrix.size() * matrix[0].size();
    vector<int> result(matrix_size);
    //{right, down, left, up}
    vector<vector<int>> directions{ {0, 1}, {1, 0}, {0, -1}, {-1, 0} };
    int direction = 0;
    int left_col = 0;
    int right_col = matrix[0].size()-1;
    int top_row = 0;
    int bottom_row = matrix.size()-1;
    int row = 0;
    int col = 0;
    int iter = 0;
    while(iter < matrix_size){
      while(col != right_col && iter < matrix_size){
        result[iter] = matrix[row][col];
        col += directions[0][1];
        ++iter;
      }
      ++top_row;
      while(row != bottom_row  && iter < matrix_size){
        result[iter] = matrix[row][col];
        row += directions[1][0];
        ++iter;
      }
      --right_col;
      while(col != left_col && iter < matrix_size){
        result[iter] = matrix[row][col];
        col += directions[2][1];
        ++iter;
      }     
      --bottom_row;
      while(row != top_row && iter < matrix_size){
        result[iter] = matrix[row][col];
        row += directions[3][0];
        ++iter;
      }
      ++left_col;
    } 
    return result;
  }
};

TEST(l54, t1){
  vector<vector<int>> matrix{{1,2,3},{4,5,6},{7,8,9}};
  auto solution = Solution();
  auto result = solution.spiralOrder(matrix);
  auto answer = vector<int>{1,2,3,6,9,8,7,4,5};
  EXPECT_EQ(result, answer);
}

TEST(l54, t2){
  vector<vector<int>> matrix{{1,2,3,4},{5,6,7,8},{9,10,11,12}};
  auto solution = Solution();
  auto result = solution.spiralOrder(matrix);
  auto answer = vector<int>{1,2,3,4,8,12,11,10,9,5,6,7};
  EXPECT_EQ(result, answer);
}

// I passed, and did well on the memory size. However my loop had too many if statements in it, this is the old code
//   for(uint iter=0; iter < matrix_size; ++iter){
    //   result[iter] = matrix[row][col];
    //   if(direction == 0 && col == right_col){ // right to down
    //     ++direction;
    //     ++top_row;
    //   }
    //   if(direction == 1 && row == bottom_row){ // down to left
    //     ++direction;
    //     --right_col;
    //   }
    //   if(direction == 2 && col == left_col){ // left to up
    //     ++direction;
    //     --bottom_row;
    //   }
    //   if(direction == 3 && row == top_row){ // up to right
    //     direction = 0;
    //     ++left_col;
    //   }
    //   row += directions[direction][0];
    //   col += directions[direction][1];
    // }
