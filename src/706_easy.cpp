#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

class MyHashMap{
public:
  MyHashMap() : keys(), values(){
  }

  void put(int key, int value){
    for(auto iter = keys.begin(); iter < keys.end(); ++iter){
      if(*iter == key){
        values[iter - keys.begin()] = value;
        return;
      }
    }
    keys.emplace_back(key);
    values.emplace_back(value);
  }

  int get(int key){
    for(auto iter = keys.begin(); iter < keys.end(); ++ iter){
      if(*iter == key){
        return *(values.begin() + (iter-keys.begin()));
      }
    }
    return -1;
  }

  void remove(int key){
    for(auto iter = keys.begin(); iter < keys.end(); ++ iter){
      if(*iter == key){
        keys.erase(iter);
        values.erase((values.begin() + (iter - keys.begin())));
        return;
      }
    }
  }

private:
  vector<int> keys;
  vector<int> values;
};

TEST(l2038, starter_example){
    MyHashMap obj = MyHashMap();
    int value = 2;
    int key = 1;
    obj.put(key,value);
    int param_2 = obj.get(key);
    obj.remove(key);
    
    EXPECT_EQ(param_2,2);
}
