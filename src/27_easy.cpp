#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

// This is a copy paste from a leetcode submission. It is a lesson in that I need to READ everything. 
// They were only analyzing the first k elements of the array, therefore you don't need to ERASE anything
int removeElement(vector<int>& nums, int val){
  int index{};
  for(int i = 0; i < nums.size(); i++){
    if(nums[i] != val){
      nums[index] = nums[i];
      index++;
    }
  }
  return index;
}

TEST(l2038, starter_example){
    auto nums = vector<int>{0,1,2,2,3,0,4,2};
    auto val = 2;
    auto result = removeElement(nums, val);
    EXPECT_EQ(result,5);
    EXPECT_EQ(nums.size(),5);
    cout<<val<<endl;
}
