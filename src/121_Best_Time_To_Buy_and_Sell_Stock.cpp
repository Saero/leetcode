#include <iostream> 
#include <vector>
#include <gtest/gtest.h>
#include <algorithm>
using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

class Solution{
public:
  int maxProfit(vector<int>& prices){
    if(prices.size() <=1){
      return 0;
    }
    int buy{prices[0]};
    int sell{prices[1]};
    int profit{sell - buy};
    bool sell_next{false};
    prices.erase(prices.begin());


    for(const auto& iter : prices){
    //for(int index=2; index < prices.size(); ++index){
    //  int iter = prices[index];
    //  cout<<"iter : "<< index<<endl;
      cout<<"iter : "<< iter <<endl;
      if(iter > sell){
        cout<<"\nsell updated "<<endl;
        sell_next = true;
        sell = iter;
      }
      else if(iter < buy){
        cout<<"\nbuy updated "<<endl;
        sell_next = false;
        buy = iter;
        sell = iter;
      }

      if((sell - buy) > profit && sell_next){
        profit = sell - buy;
        cout<<"\nprofit updated : "<< profit<<endl;
      }

    }
    if(profit > 0){
      return profit;
    }
    return 0;
  }
};


TEST(_45_Jump_Game_II, starter_example){
    Solution solution{};
    vector<int> nums{7,1,5,3,6,4};
    auto result = solution.maxProfit(nums);
    EXPECT_EQ(result, 5);

    nums = {7,6,4,3,1};
    result = solution.maxProfit(nums);
    EXPECT_EQ(result, 0);

    nums = {7,2,4,1};
    result = solution.maxProfit(nums);
    EXPECT_EQ(result, 2);

    //vector<int> nums{2,4,1};
    //auto result = solution.maxProfit(nums);
    nums = {2,4,1};
    result = solution.maxProfit(nums);
    EXPECT_EQ(result, 2);

}

// The below version cannot deal with [7,2,4,1]...where the answer is somewhere in the middle of the set
//  int maxProfit(vector<int>& prices){
//    // case 1: min comes before max, this is the easy case
//    auto min = min_element(prices.begin(), prices.end()-1);
//    auto max = max_element(prices.begin()+1, prices.end());
//    auto min_day = distance(prices.begin(), min);
//    cout<<"\nmin_day "<<min_day<<endl;
//    auto max_day = distance(prices.begin(), max);
//    cout<<"\nmax_day "<<max_day<<endl;
//    if(max_day > min_day && (*max - *min) > 0){// day to sell is after day to buy
//      cout<<" \n Case 1 triggered"<<endl;
//      return (*max - *min);
//    }
//    else{
//      cout<<" \n Case 2 triggered"<<endl;
//      // case 2: min comes after max, this is where things get interesting
//      int delta{0};// the best profit that can be attained
//      // case 2.1 : min of set before max yields highest difference
//      auto min_pre_max_day = min_element(prices.begin(), max);
//      cout<<"\nmin_pre_max_day = "<<*min_pre_max_day<<endl;
//      auto max_subsetting{(*max - *min_pre_max_day)};
//      cout<<"\nmax_subsetting = "<<max_subsetting<<endl;
//      if(max_subsetting > 0){
//        delta = max_subsetting;
//      }
//      cout<<"delta 1: "<<delta<<endl;
//      // case 2.2 : max of set ofter min yields highest difference
//      auto max_post_min_day = max_element(min, prices.end());
//      cout<<"\nmax_post_min_day = "<<*max_post_min_day<<endl;
//      auto min_subsetting{(*max_post_min_day - *min)};
//      cout<<"\nmin_subsetting = "<<min_subsetting<<endl;
//      if( min_subsetting > 0 && min_subsetting > delta){
//        delta = min_subsetting;
//      }
//      cout<<"delta 2: "<<delta<<endl;
//      if(delta > 0){
//        return delta;
//      }
//      cout<<"delta 3: "<<delta<<endl;
//    }
//    return 0;
//  }



// this is the leetcode 0ms solution....what did they do in there???
//
//int init = [] {
//	cin.tie(nullptr)->sync_with_stdio(false);
//    freopen("user.out", "w", stdout);
//
//    for (string s; getline(cin, s); cout << '\n') {
//        int solution = 0, minPrice = INT_MAX;
//        for (int _i = 1, _n = s.length(); _i < _n; ++_i) {
//            int price = s[_i++] & 15;
//            while ((s[_i] & 15) < 10) price = price * 10 + (s[_i++] & 15);
//            minPrice = min(minPrice, price);
//            solution = max(solution, price - minPrice);
//        }
//        cout << solution;
//    }
//    //exit(0);
//    return 0;
//}();
//
//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        return 0;
//    }
//};
