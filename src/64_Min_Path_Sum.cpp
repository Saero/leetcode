#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

int minPath(vector<vector<int>>& grid){
  auto height{grid.size()};
  auto width {grid[0].size()};
  int posh{0};
  int posw{0};
  int upperl{numeric_limits<int>::max()};
  for(int i{0}; i < height; ++i){
    for(int j{0}; j < width; ++i){

      if(i==0 && j==0){continue;}//nothing to do for the first one
      if((i-1)>=0){ posh = grid[i-1][j]; }
      else{ posh = upperl; }
      if((j-1)>=0){ posw = grid[i][j-1]; }
      else{ posw = upperl; }
      
      grid[i][j] = (posw < posh) ? posw : posh;
    }
  }
  return 0; 
}

TEST(l64, starter_example){
    vector<vector<int>> grid{{1,3,1},{1,5,1},{4,2,1}};
    auto result = minPath(grid);
    EXPECT_EQ(result, 12);
}
