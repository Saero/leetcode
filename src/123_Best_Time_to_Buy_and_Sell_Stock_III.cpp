#include <iostream> 
#include <vector>
#include <gtest/gtest.h>
#include <algorithm>
#include <iterator>
using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container){
        cout<<iter<< " ";
    }
    cout<<endl;
}

template<typename T>
void printTable(string name, T& container){
    cout<<name<<" "<<endl;
    for(auto iter : container){
      for(auto iter2 : iter){
        cout<<iter2<< " ";
      }
      cout<<endl;
    }
    cout<<endl;
}
// so I want a table that goes by day, but records profit in its indicies, 
// profit will be described as profit by the diff of prices on d2-d1
// do not engage in any trade that is below 0
// if a trade is taken up as the best, than it must be better than any of the 
// previous pair
// table:
// day 1  2  3  4  5
// 1   p1 
// 2
// 3
// 4
// 5


//      sd1, sd2, sd3
// bd1  0    p2   p3
// bd2  0    0    p1
// bd3  0    0     0
//
// searching for max profit: 
//    if max 1 is updated, zero out max 2
//    max 2 index cannot have a buy date before max 1's sell date

// This is not the correct solution
// This fails to compute the whole range of profit combinations


class Solution{
  public:
  int maxProfit(vector<int>& prices){
    if(prices.size()==0) // if no prices given
      return 0; 

    vector<vector<int>> profit(prices.size(), vector<int>(prices.size(),0));
    // populate profit
    for(size_t buy{}; buy < prices.size(); ++buy){
      for(size_t sell{}; sell < prices.size(); ++sell){
        if(sell<=buy)
          continue;
        profit[buy][sell] = prices[sell] - prices[buy];
      }
    }
    printTable("profit table", profit);
  
    int max1{0};
    int max2{0};
    int max1b{0};
    int max1s{0};
    for(size_t buy{}; buy < prices.size(); ++buy){
      for(size_t sell{}; sell < prices.size(); ++sell){
        if(profit[buy][sell] > max1){
          max1 = profit[buy][sell];
          max2 = 0;
          max1b = buy;
          max1s = sell;
          cout<<"max1 updated: "<<max1<< "  max1b "<< max1b<< "  max1s "<<max1s<<endl;
        }
        else if(buy >= max1s && profit[buy][sell] > max2){
          max2 = profit[buy][sell];
          cout<<"max2 updated: "<<max2<<endl;
        }
      }
    }
    return max1+max2;
  }
};

TEST(_123, starter_example0){
    Solution solution{};
    vector<int> prices{3,3,5,0,0,3,1,4};
    auto result = solution.maxProfit(prices);
    EXPECT_EQ(result, 6);
}

//TEST(_123, starter_example1){
//    Solution solution{};
//    vector<int> prices{1,2,3,4,5};
//    auto result = solution.maxProfit(prices);
//    EXPECT_EQ(result, 4);
//}
//
//TEST(_123, starter_example2){
//    Solution solution{};
//    vector<int> prices{7,6,4,3,1};
//    auto result = solution.maxProfit(prices);
//    EXPECT_EQ(result, 0);
//}
