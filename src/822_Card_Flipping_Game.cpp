#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

int flipgame(vector<int>& fronts, vector<int>& backs){
    //What I want to do is return the minimum number that is only present
    // on one card side

    // get all of the bad cards that will not contribute to the solution
    vector<int> badCards;
    for(int iter = 0; iter < fronts.size(); ++iter){
        if(fronts[iter] == backs[iter])
            badCards.emplace_back(fronts[iter]);
    }
    // remove all of the cards that will not contribute to the solution
    for(auto& badCard : badCards){
        fronts.erase(remove(fronts.begin(), fronts.end(), badCard), fronts.end());
        backs.erase(remove(backs.begin(), backs.end(), badCard), backs.end());
    }
    // sort the remaining cards
    sort(fronts.begin(), fronts.end());
    sort(backs.begin(), backs.end());
    //printIterable("Setbad: ",   badCards);
    //printIterable("Setfront: ", fronts);
    //printIterable("Setback: ",  backs);

    for(int iter = 0; iter < fronts.size()+backs.size(); ++iter){
        cout<<" fronts: "<<fronts[iter]<<" backs: "<<backs[iter]<<endl;
        if(!fronts.empty()){
            if((!backs.empty() && fronts[iter] <= backs[iter]) || backs.empty()){
                    return fronts[iter];
            }
        }
        if(!backs.empty()){
            if((!fronts.empty() && fronts[iter] > backs[iter]) || fronts.empty()){
                    return backs[iter];
            }
        }
    }
    return 0;
}

TEST(l822, starter_example){
    auto fronts = vector<int>{1,2,4,4,7};
    auto backs =  vector<int>{1,3,4,1,3};
    auto result = flipgame(fronts, backs);
    EXPECT_EQ(result, 2);
}

TEST(l822, failed_test_1){
    auto fronts = vector<int>{1};
    auto backs =  vector<int>{2};
    auto result = flipgame(fronts, backs);
    EXPECT_EQ(result, 1);
}

TEST(l822, failed_test_2){
    auto fronts = vector<int>{1,2};
    auto backs =  vector<int>{2,1};
    auto result = flipgame(fronts, backs);
    EXPECT_EQ(result, 1);
}

TEST(l822, failed_test_3){
    auto fronts = vector<int>{1,1};
    auto backs =  vector<int>{1,2};
    auto result = flipgame(fronts, backs);
    EXPECT_EQ(result, 2);
}

TEST(l822, failed_test_4){
    auto fronts = vector<int>{4,1,5,3,4};
    auto backs =  vector<int>{2,1,1,1,5};
    auto result = flipgame(fronts, backs);
    EXPECT_EQ(result, 2);
}
