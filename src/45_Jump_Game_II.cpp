#include <iostream> 
#include <vector>
#include <gtest/gtest.h>
#include <algorithm>
using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

int min_element_(vector<int>& container ,int iter1, int iter2){
  int min=std::numeric_limits<int>::max();
  if(iter2 >= container.size()){
    iter2 = container.size() - 1;
  }
  for(int iter = iter1; iter <= iter2; ++iter){
    if(container[iter] < min){
      min = container[iter];
    }
  }
  return min;
}

class Solution{
public:
  int jump(vector<int>& nums){
    vector<int> jumps(nums.size(), std::numeric_limits<int>::max() - 1);
    if(nums.size() == 1){
      return 0;
    }
    nums.back() = 0;
    jumps.back() = 0;
    for(int iter = nums.size() - 1; iter >= 0; --iter){
      //int temp = min_element(nums.end()-iter, nums.end() - iter + nums[iter])[0] + 1;
      int temp = min_element_(jumps, iter, iter + nums[iter]) + 1;
      //cout<<"iter: "<<iter<<" nums[iter]: "<<nums[iter] <<" index1 raw : "<<iter<<" index2 raw : "<<(iter + nums[iter])<<endl;
      //printIterable("nums", nums);
      //cout<<"temp is: "<<temp<<endl;
      if (temp < jumps[iter]){
        jumps[iter] = temp;
      }
      //printIterable("Jumps", jumps);
      //cout<<endl;
    }
    //cout<<"\n\n"<<endl;
    return jumps[0];
  }
};


TEST(_45_Jump_Game_II, starter_example){
    Solution solution{};
    vector<int> nums{2,3,0,1,4};
    auto result = solution.jump(nums);
    EXPECT_EQ(result, 2);

    vector<int> nums2{2,1};
    auto result2 = solution.jump(nums2);
    EXPECT_EQ(result2, 1);
}
