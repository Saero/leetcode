#include <iostream> 
#include <vector>
#include <gtest/gtest.h>
#include <algorithm>
using namespace std;//bad code, but needed as that is what leetcode operates on


// I didn't copy in my solution, but basically it's y=mx+b, extrapolate forward and backward, interpolate between points. Then to bisection search between the two points

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

class Solution{
public:
  int maxProfit(vector<int>& prices){
    return 0;
  }
};


TEST(ctc_screening, starter_example){
    Solution solution{};
    vector<int> nums{7,1,5,3,6,4};
    auto result = solution.maxProfit(nums);
    EXPECT_EQ(result, 0);
}

