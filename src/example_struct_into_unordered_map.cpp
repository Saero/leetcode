#include <iostream> 
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>
#include <unordered_map>
#include <utility>

// This is what I got stuck on during an interview

using namespace std;//bad code, but needed as that is what leetcode operates on

template<typename T>
void printIterable(string name, T& container){
    cout<<name<<" ";
    for(auto iter : container)
        cout<<iter<< " ";
    cout<<endl;
}

int struct_into_unordered_map(vector<int>& nums){
    struct job{
        int jobId;
        int runtime;
        int nextJobId;
    };

    job parsedJob{nums[0], nums[1], nums[2]};

    unordered_map<int, struct job> inputCsv{};

    inputCsv.insert({parsedJob.jobId, std::move(parsedJob)});

    return true;
}

TEST(struct_into_unordered_map, starter_example){
    auto nums = vector<int>{0,1,2};
    auto result = struct_into_unordered_map(nums);
    EXPECT_EQ(result, true);
}
